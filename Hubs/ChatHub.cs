using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;


namespace ChatSample.Hubs {
    public class ChatHub : Hub {

        private RoomsManager _roomsManager;

        public ChatHub(RoomsManager roomsManager) {
            _roomsManager = roomsManager;
        }

        public async Task Send(string roomId, string user, string message) {
            //await Clients.User.SendAsync("ReceiveMessage", user, message);
            var sessions = _roomsManager.GetConnections(roomId);
            if(sessions != null) {
                //Console.WriteLine(user + ", " + String.Join(", ", sessions.ToArray()));
                await Clients.Clients(sessions)
                             .SendAsync("ReceiveMessage", user, message);
                
            } else {
                // TODO: throw error
                await Clients.Client(Context.ConnectionId).SendAsync("AdminMessage", "No room found");
            }
        }

        public async override Task OnConnectedAsync() {
            //await Clients.All.SendAsync("AdminMessage", $"{Context.ConnectionId} connected");
            //_roomsManager.currentConnections[Context.ConnectionId] = 0;
            await base.OnConnectedAsync();
        }

        public async override Task OnDisconnectedAsync(Exception? ex) {
            //await Clients.All.SendAsync("AdminMessage", $"{Context.ConnectionId} disconnected");  
            byte b = 0;
            _roomsManager.currentConnections.TryRemove(Context.ConnectionId, out b);
            string? roomId = _roomsManager.RemoveConnection(Context.ConnectionId);
            if(roomId != null) {
                var sessions = _roomsManager.GetConnections(roomId);
                if(sessions != null) {
                    await Clients.Clients(sessions)
                             .SendAsync("AdminMessage", $"{Context.ConnectionId} disconnected");
                }
            } 
            await base.OnDisconnectedAsync(ex);
        }

        public Task JoinRoom(string roomId, string playerId) {
            // todo error messages / exception catching
            Console.WriteLine(playerId + " joining " + roomId);
            _roomsManager.AddOrUpdatePlayer(roomId, playerId, Context.ConnectionId);
            return Task.CompletedTask;
        }

    }
}







