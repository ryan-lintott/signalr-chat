using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;


namespace ChatSample {
    public class Program {

        private static CancellationTokenSource gracefulShutdownToken = new System.Threading.CancellationTokenSource();

        public static int Main(string[] args) {

            CreateHostBuilder(args).Build().RunAsync(gracefulShutdownToken.Token).Wait();

            return 0;
        }
        
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => {
                    webBuilder.UseStartup<Startup>().UseUrls("http://0.0.0.0:5000");
                });
    }
}