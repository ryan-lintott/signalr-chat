
using System.Collections.Concurrent;
using System.Threading;
using System.Net.Http;
using System.Net;
using Microsoft.Extensions.Logging;
using System.IO;
//using System.Web;

public class RoomsManager {

    int counter = 0;

    int roomLimit;

    public int RoomLimit {
        get { return roomLimit; }
    }

    private ConcurrentDictionary<string, Room> rooms = new ConcurrentDictionary<string, Room>();
    
    private ConcurrentDictionary<string, string> connectionIdToRoomId = new ConcurrentDictionary<string, string>();

    // TODO: get logging working with unit tests
    //private readonly ILogger<RoomsManager> _logger;
    // private Timer? _timer = null;

    private int connectionIdCleanupPeriod = 300; // seconds
    
    public ConcurrentDictionary<string, byte> currentConnections = new ConcurrentDictionary<string, byte>();

    public RoomsManager() {
        // _timer = new Timer(CleanupConnectionIds, null, TimeSpan.FromSeconds(connectionIdCleanupPeriod), 
        //         TimeSpan.FromSeconds(connectionIdCleanupPeriod));
        //_logger = logger;
        if(!Int32.TryParse(Environment.GetEnvironmentVariable("MAX_ROOMS"), out roomLimit)) {
            roomLimit = 100;
        }

        for(int i = 0; i < 25; i++) {
            CreateRoom();
        }
    }

    public void AddOrUpdatePlayer(string roomId, string playerId, string connectionId) {
        Room? room = null;
        rooms.TryGetValue(roomId, out room);
        
        string? connectionRoomId = null;

        lock(connectionIdToRoomId) {
            connectionIdToRoomId.TryGetValue(connectionId, out connectionRoomId);
            
            // TODO, add exceptions 
            if(connectionRoomId != null && !connectionRoomId.Equals(roomId)) {
                throw new Exception($"Player is already in a different room, (room {connectionRoomId})");
            }
            
            if(room != null) {
                bool succeeded = room.AddOrUpdatePlayer(playerId, connectionId);
                if(succeeded) {
                    connectionIdToRoomId[connectionId] = roomId;
                }
            } else {
                //return false;
            }
        }
    }

    public string? RemoveConnection(string connectionId) {
        string? roomId = null;
        bool success = false;
        lock(connectionIdToRoomId) {
            connectionIdToRoomId.TryGetValue(connectionId, out roomId);
            if(roomId != null) {
                Room? room = null;
                rooms.TryGetValue(roomId, out room);
                if(room != null) {
                    success = room.RemoveConnection(connectionId);
                }
                connectionIdToRoomId.TryRemove(connectionId, out roomId);
            } 
        }
        if(success) {
            return roomId;
        } else {
            return null;
        }
    }

    public List<string>? GetConnections(string roomId) {
        Room? room = null;
        rooms.TryGetValue(roomId, out room);
        if(room != null) {
            return room.GetConnections();
        } else {
            return null;
        }
    }

    public string CreateRoom() {
        int newVal = Interlocked.Add(ref counter, 1);
        if(newVal > roomLimit) {
            throw new Exception("exceeded max number of rooms");
        }
        rooms.TryAdd(newVal.ToString(), new Room());
        return newVal.ToString();
    }

    public bool DestroyRoom(string roomId) {
        Room? room = null;
        return rooms.TryRemove(roomId, out room);
    }

    /*
        Calling this clean up function for the connectionIdToRoomId dictionary every x minutes
        I think there's a possibility of connectionIds left dangling in
        connectionIdToRoomId, due to multithreading

        currentConnections dictionary is keeping track of connections more reliably and
        this fn can run every so often to clean up any possible dangling connection id references

        Should have minimal performance impact....

        note this might not be the right approach
    */
    // public void CleanupConnectionIds(object? state) {
    //     // TODO: get logging working
    //     //_logger
    //     Console.WriteLine("Cleaning up connectionIdToRoomId");
    //     var connectionsToRemove = connectionIdToRoomId.Where(f => !currentConnections.ContainsKey(f.Key)).ToArray();
    //     foreach(var connId in connectionsToRemove) {
    //         string? roomId = null;
    //         Console.WriteLine("Dangling connectionId found!");
    //         connectionIdToRoomId.TryRemove(connId.Key, out roomId);
    //     }
    // }

}