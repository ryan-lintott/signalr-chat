using System.Collections.Concurrent;

class Room {

    // TODO: the c# get, set syntax
    ConcurrentDictionary<string, string> playerIdToConnectionId = new ConcurrentDictionary<string, string>();

    public int maxPlayers = 4;

    public int GetPlayerCount() {
        return playerIdToConnectionId.Count;
    }

    public bool AddOrUpdatePlayer(string playerId, string connectionId) {
        lock(playerIdToConnectionId) {
            if(playerIdToConnectionId.Count >= 4 && !playerIdToConnectionId.ContainsKey(playerId)) {
                // TODO: custom exceptions maybe
                throw new Exception("exceeded max player count");
            }
            playerIdToConnectionId[playerId] = connectionId;   
        }
        return true;
    }

    public bool RemoveConnection(string connectionId) {
        bool result = false;

        foreach(KeyValuePair<string, string> entry in playerIdToConnectionId) {
            if(entry.Value.Equals(connectionId)) {
                result = playerIdToConnectionId.TryUpdate(entry.Key, "", connectionId);
                break;
            }
        }
        return result;
    }

    public List<string> GetConnections() {
        return playerIdToConnectionId.Values.ToList();
    }
}