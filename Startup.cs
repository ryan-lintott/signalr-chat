using ChatSample.Hubs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.SignalR.Client;
using System.Net.Http;
using System.Net; 

namespace ChatSample {
    public class Startup {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services) {
            services.AddHostedService<AgonesSdkService>();
            services.AddSingleton<RoomsManager>();
            services.AddSignalR().AddHubOptions<ChatHub>(options => {
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {

            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }
            app.UseFileServer();
            app.UseRouting();


            app.UseEndpoints(endpoints => {
                endpoints.MapHub<ChatHub>("/chat");

                // TODO: service for this (?)
                // TODO: error handling
                endpoints.MapPost("/gameserver", (RoomsManager roomsManager) => {
                    Dictionary<string, string> result = new Dictionary<string, string>();
                    try {
                        result.Add("roomId", roomsManager.CreateRoom());  
                        return result;       
                    } catch(Exception ex) {
                        return result;
                    }
                       
                });                 


            });


            // HubConnection conn = new HubConnectionBuilder()
            //     .WithUrl("http://0.0.0.0:5000/chat")
            //     .Build();
        }
    }
}